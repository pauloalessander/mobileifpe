import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
import FlashMessage, { showMessage } from "react-native-flash-message";

function EditCadastroScreen({route, navigation}) {
  const [getNome,setNome] = useState([]);
  const [getEmail,setEmail] = useState([]);
  const [getTelefone,setTelefone] = useState([]);
  const [getId, setId] = useState([]);
  const [getCpf, setCpf] = useState([]);

  useEffect(()=>{
    if(route.params){
      const { nome } =  route.params 
      const { telefone } =  route.params 
      const { email } =  route.params
      const { id } =  route.params
      const { cpf } =  route.params

      setNome(nome)
      setTelefone(telefone)
      setCpf(cpf)
      setEmail(email)
      setId(id)
    }
  },[])
  
  async function put() {
    await axios.put('http://professornilson.com/testeservico/clientes/' + getId,{
      nome: getNome,
      email: getEmail,
      telefone: getTelefone
    }
     )
     .then((response) => {
        showMessage({
          message: "Registro alterado com sucesso!",
          type: "success"
        });
       console.log(response);
     })
     .catch((error) => {
        showMessage({
          message: "Algum erro aconteceu!",
          type: "warning"
        });
        console.log(error);
     });
  }

  function remove() {
    axios.delete('http://professornilson.com/testeservico/clientes/' + getId)
      .then((response) => {
          showMessage({
              message: "Registro excluído com sucesso!",
              type: "success",
          });
          navigation.navigate('Contatos');
          console.log(response);
      })
      .catch((error) => {
          showMessage({
              message: "Algum erro aconteceu!",
              type: "warning",
          });
          console.log(error);
      });
}
  
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ffffff' }}>
      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Nome</Text>
      <Input style={style.container} placeholder="Nome" value={getNome||''} onChangeText={(texto) => setNome(texto)} />

      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Email</Text>
      <Input style={style.container} placeholder="Email" value={getEmail||''} onChangeText={(texto) => setEmail(texto)} />

      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Telefone</Text>
      <Input style={style.container} placeholder="Telefone" value={getTelefone||''} onChangeText={(texto) => setTelefone(texto)} />
      
      
      <Button
        title={'Alterar'}
        onPress={() => put()}
        containerStyle={{
          width: 200,
          marginHorizontal: 50,
          marginVertical: 10,
          borderRadius:10
        }}
      />

      <Button
        title={'Excluir'}
        onPress={() => remove()}
        buttonStyle={{backgroundColor: 'red'}}
        containerStyle={{
          width: 200,
          marginHorizontal: 50,
          marginVertical: 10,
          borderRadius:10
        }}
      />

      <FlashMessage position="top" />
    </View>
  );
}


const style = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#efefee"
  },
  title: {
    fontSize: 12,
    color: "#000",
  }
})

export default EditCadastroScreen;