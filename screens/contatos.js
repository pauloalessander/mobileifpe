import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { ListItem, Avatar } from 'react-native-elements';
import axios from 'axios';

const perfil = require('../assets/perfil.png')

function ContatosScreen({navigation}) {
  const [listaContatos, setListaContatos] = useState([]);
  
  useEffect(()=>{
        
    async function get(){
        const result = await axios(
            'http://professornilson.com/testeservico/clientes',
          );
          setListaContatos(result.data);
    }
    get();
  }) 
  
  return (      
        <View>
            {
                listaContatos.map((l, i) => (
                <ListItem key={i} bottomDivider 
                  onPress={()=>navigation.navigate('Editar',{
                  nome:l.nome,
                  telefone:l.telefone,
                  email:l.email
               })}>
                    <Avatar rounded source={{uri: perfil}} />
                    <ListItem.Content>
                    <ListItem.Title>{l.nome}</ListItem.Title>
                    <ListItem.Subtitle>{l.telefone}</ListItem.Subtitle>
                    <ListItem.Subtitle>{l.email}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
                ))
            }
        </View>        
    );
}

export default ContatosScreen;
