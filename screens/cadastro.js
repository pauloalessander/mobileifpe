import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
import { useState } from 'react';
import axios from 'axios';

function CadastroScreen() {
  
  const [getNome,setNome] = useState([]);
  const [getTelefone,setTelefone] = useState([]);
  const [getEmail,setEmail] = useState([]);

  function insert() {
    axios.post('http://professornilson.com/testeservico/clientes', {
      nome: getNome,
      telefone: getTelefone,
      cpf: getEmail
    })
      .then((response) => {
        console.log(response.config.data);
        navigation.navigate('Contatos')
        console.log('Contato cadastrado com sucesso')
      })
      .catch((error) => {
        console.log(error);
        console.log('Erro no cadastro do contato')
      });
  }
  
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ffffff' }}>
      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Nome</Text>
      <Input style={style.container} onChangeText={(texto) => setNome(texto)} />

      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Email</Text>
      <Input style={style.container} onChangeText={(texto) => setEmail(texto)} />

      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Telefone</Text>
      <Input style={style.container} onChangeText={(texto) => setTelefone(texto)} />
      
      
      <Button
        title={'Salvar'}
        onPress={() => insert()}
        containerStyle={{
          width: 200,
          marginHorizontal: 50,
          marginVertical: 10,
          borderRadius:10
        }}
      />
    </View>
  );
}


const style = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#efefee"
  },
  title: {
    fontSize: 12,
    color: "#000",
 }
})

export default CadastroScreen;