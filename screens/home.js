import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button, Image } from 'react-native-elements';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { useState } from 'react';
import { initializeApp } from "firebase/app";
import FlashMessage, { showMessage } from "react-native-flash-message";

const staticImage = require("../assets/residence_fountain_fountain_water.jpg");
function HomeScreen({navigation}) {
  // firebase
  const firebaseConfig = {
    apiKey: "AIzaSyAwFULWgnR_h1yAgx8epGbNqHA5oZR_RQs",
    authDomain: "aula-app-baccb.firebaseapp.com",
    projectId: "aula-app-baccb",
    storageBucket: "aula-app-baccb.appspot.com",
    messagingSenderId: "50525201513",
    appId: "1:50525201513:web:218b8bc9baf354c0dd6ef8",
    measurementId: "G-BQJ7NGQ46H"
  };
  
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);

  const [getEmail,setEmail] = useState([]);
  const [getSenha,setSenha] = useState([]);
  const auth = getAuth();

  // função para login
  function login(){
    signInWithEmailAndPassword(auth, getEmail, getSenha)
    .then((userCredential) => {
      navigation.navigate('Contatos')
      const user = userCredential.user;
      console.log(user);
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorCode);
      if (errorCode === 'auth/wrong-password') {
        showMessage({
          message: 'Senha incorreta!',
          type: "warning"
        });
      } else if (errorCode === 'auth/invalid-email') {
        showMessage({
          message: 'Email incorreto!',
          type: "warning"
        });
      } else {
        showMessage({
          message: 'Algum erro aconteceu!',
          type: "danger"
        });
      }
    });
  }
  
  return (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ffffff' }}>

    <Image
      source={{ uri: staticImage }}
      style={{ width: 100, height: 100, borderRadius: 100}}
    />

    <Text style={
      {
        alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
      }
    }>Email</Text>
    <Input style={style.container} onChangeText={(texto) => setEmail(texto)} />

    <Text style={
      {
        alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
      }
    }>Senha</Text>
    <Input style={style.container} secureTextEntry={true} onChangeText={(texto) => setSenha(texto)} />

    <Button
      title={'Login'}
      onPress={()=>login()}
      containerStyle={{
        width: 200,
        marginHorizontal: 50,
        marginVertical: 10,
        borderRadius:10
      }}
    />

    <Button
      title={'Cadastre-se'}
      onPress={()=>navigation.navigate('UsuarioCadastro')}
      containerStyle={{
        width: 200,
        marginHorizontal: 50,
        marginVertical: 10,
        borderRadius:10
      }}
    />

    <FlashMessage position="top" />
  </View>
  );
}

const style = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#efefee"
  },
  title: {
    fontSize: 12,
    color: "#000",
 }
})

export default HomeScreen;