import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";

function UsuarioCadastroScreen() {
  // firebase
  const firebaseConfig = {
    apiKey: "AIzaSyAwFULWgnR_h1yAgx8epGbNqHA5oZR_RQs",
    authDomain: "aula-app-baccb.firebaseapp.com",
    projectId: "aula-app-baccb",
    storageBucket: "aula-app-baccb.appspot.com",
    messagingSenderId: "50525201513",
    appId: "1:50525201513:web:218b8bc9baf354c0dd6ef8",
    measurementId: "G-BQJ7NGQ46H"
  };
  
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  
  const [getEmail,setEmail] = useState([]);
  const [getSenha,setSenha] = useState([]);
  const auth = getAuth();

  function insert(){
    createUserWithEmailAndPassword(auth, getEmail, getSenha)
    .then((userCredential) => {
      const user = userCredential.user;
      console.log(user);
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorCode, errorMessage);
    });
  }

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ffffff' }}>

      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Email</Text>
      <Input style={style.container} onChangeText={(texto) => setEmail(texto)} />

      <Text style={
        {
          alignSelf: 'flex-start', fontWeight: 'bold', paddingLeft: 10
        }
      }>Senha</Text>
      <Input secureTextEntry={true} style={style.container} onChangeText={(texto) => setSenha(texto)} />
      
      
      <Button
        title={'Salvar'}
        onPress={() => insert()}
        containerStyle={{
          width: 200,
          marginHorizontal: 50,
          marginVertical: 10,
          borderRadius:10
        }}
      />
    </View>
  );
}


const style = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#efefee"
  },
  title: {
    fontSize: 12,
    color: "#000",
 }
})

export default UsuarioCadastroScreen;